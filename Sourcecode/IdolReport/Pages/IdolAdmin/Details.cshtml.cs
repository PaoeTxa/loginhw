using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolReport.Data;
using IdolReport.Models;

namespace IdolReport.Pages.IdolAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly IdolReport.Data.IdolReportContext _context;

        public DetailsModel(IdolReport.Data.IdolReportContext context)
        {
            _context = context;
        }

        public IdolEvent IdolEvent { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            IdolEvent = await _context.IdolList
                .Include(i => i.IdolCat)
                .Include(i => i.postUser).FirstOrDefaultAsync(m => m.IdolEventID == id);

            if (IdolEvent == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
