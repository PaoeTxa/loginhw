using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using IdolReport.Data;
using IdolReport.Models;

namespace IdolReport.Pages.IdolAdmin
{
    public class CreateModel : PageModel
    {
        private readonly IdolReport.Data.IdolReportContext _context;

        public CreateModel(IdolReport.Data.IdolReportContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["IdolCategoryID"] = new SelectList(_context.IdolCateg, "IdolCategoryID", "IdolCategoryID");
        ViewData["IdolUserid"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public IdolEvent IdolEvent { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.IdolList.Add(IdolEvent);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}