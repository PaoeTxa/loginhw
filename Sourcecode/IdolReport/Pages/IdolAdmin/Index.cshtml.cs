using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolReport.Data;
using IdolReport.Models;

namespace IdolReport.Pages.IdolAdmin
{
    public class IndexModel : PageModel
    {
        private readonly IdolReport.Data.IdolReportContext _context;

        public IndexModel(IdolReport.Data.IdolReportContext context)
        {
            _context = context;
        }

        public IList<IdolEvent> IdolEvent { get;set; }

        public async Task OnGetAsync()
        {
            IdolEvent = await _context.IdolList
                .Include(i => i.IdolCat)
                .Include(i => i.postUser).ToListAsync();
        }
    }
}
