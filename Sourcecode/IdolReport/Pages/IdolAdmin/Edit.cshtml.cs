using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IdolReport.Data;
using IdolReport.Models;

namespace IdolReport.Pages.IdolAdmin
{
    public class EditModel : PageModel
    {
        private readonly IdolReport.Data.IdolReportContext _context;

        public EditModel(IdolReport.Data.IdolReportContext context)
        {
            _context = context;
        }

        [BindProperty]
        public IdolEvent IdolEvent { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            IdolEvent = await _context.IdolList
                .Include(i => i.IdolCat)
                .Include(i => i.postUser).FirstOrDefaultAsync(m => m.IdolEventID == id);

            if (IdolEvent == null)
            {
                return NotFound();
            }
           ViewData["IdolCategoryID"] = new SelectList(_context.IdolCateg, "IdolCategoryID", "IdolCategoryID");
           ViewData["IdolUserid"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(IdolEvent).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IdolEventExists(IdolEvent.IdolEventID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool IdolEventExists(int id)
        {
            return _context.IdolList.Any(e => e.IdolEventID == id);
        }
    }
}
