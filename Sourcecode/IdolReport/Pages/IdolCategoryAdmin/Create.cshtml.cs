using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using IdolReport.Data;
using IdolReport.Models;

namespace IdolReport.Pages.IdolCategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly IdolReport.Data.IdolReportContext _context;

        public CreateModel(IdolReport.Data.IdolReportContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public IdolCategory IdolCategory { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.IdolCateg.Add(IdolCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}