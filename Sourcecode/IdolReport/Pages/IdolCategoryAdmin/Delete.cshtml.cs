using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolReport.Data;
using IdolReport.Models;

namespace IdolReport.Pages.IdolCategoryAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly IdolReport.Data.IdolReportContext _context;

        public DeleteModel(IdolReport.Data.IdolReportContext context)
        {
            _context = context;
        }

        [BindProperty]
        public IdolCategory IdolCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            IdolCategory = await _context.IdolCateg.FirstOrDefaultAsync(m => m.IdolCategoryID == id);

            if (IdolCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            IdolCategory = await _context.IdolCateg.FindAsync(id);

            if (IdolCategory != null)
            {
                _context.IdolCateg.Remove(IdolCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
