using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolReport.Data;
using IdolReport.Models;

namespace IdolReport.Pages.IdolCategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly IdolReport.Data.IdolReportContext _context;

        public IndexModel(IdolReport.Data.IdolReportContext context)
        {
            _context = context;
        }

        public IList<IdolCategory> IdolCategory { get;set; }

        public async Task OnGetAsync()
        {
            IdolCategory = await _context.IdolCateg.ToListAsync();
        }
    }
}
