using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IdolReport.Data;
using IdolReport.Models;

namespace IdolReport.Pages.IdolCategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly IdolReport.Data.IdolReportContext _context;

        public EditModel(IdolReport.Data.IdolReportContext context)
        {
            _context = context;
        }

        [BindProperty]
        public IdolCategory IdolCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            IdolCategory = await _context.IdolCateg.FirstOrDefaultAsync(m => m.IdolCategoryID == id);

            if (IdolCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(IdolCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IdolCategoryExists(IdolCategory.IdolCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool IdolCategoryExists(int id)
        {
            return _context.IdolCateg.Any(e => e.IdolCategoryID == id);
        }
    }
}
