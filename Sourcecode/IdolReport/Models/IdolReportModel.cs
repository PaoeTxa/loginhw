using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace IdolReport.Models{

    public class IdolUser : IdentityUser{
        public string FirstName {get; set;}
        public string LastName {get; set;}

    }
    public class IdolCategory{
        public int IdolCategoryID {get; set;}
        public string ShortName {get; set;}
        public string Fullname {get; set;}
    }
    public class IdolEvent{
        public int IdolEventID {get; set;}

        public int IdolCategoryID {get; set;}
        public IdolCategory IdolCat {get; set;}

        [DataType(DataType.Date)]
        public string ReportDate {get; set;}
        public string IdolDetail {get; set;}

        public string IdolUserid {get; set;}
        public IdolUser postUser {get; set;}
    }
}