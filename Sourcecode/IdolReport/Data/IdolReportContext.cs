using IdolReport.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace IdolReport.Data{
    public class IdolReportContext : IdentityDbContext<IdolUser>
    {
        public DbSet<IdolEvent> IdolList {get; set;}
        public DbSet<IdolCategory> IdolCateg {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder){
            optionsBuilder.UseSqlite(@"Data source=IdolReport.db");
        }
    }
}